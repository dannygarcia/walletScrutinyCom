---
wsId: swissBitcoinPay
title: Swiss Bitcoin Pay
altTitle: 
authors:
- danny
- keraliss
users: 1000
appId: ch.swissbitcoinpay.checkout
appCountry: 
released: 2022-11-15
updated: 2025-02-03
version: 2.3.7
stars: 4.7
ratings: 
reviews: 
size: 
website: https://swiss-bitcoin-pay.ch
repository: https://github.com/SwissBitcoinPay/app
issue: https://github.com/SwissBitcoinPay/app/issues/53
icon: ch.swissbitcoinpay.checkout.png
bugbounty: 
meta: ok
verdict: nonverifiable
appHashes: []
date: 2024-09-18
signer: 17d9c0bf025008da16d5a146e1beaca6ddcfe3cb0cf063da23c847d3007eb621
reviewArchive:
- date: 2024-08-23
  version: 2.1.1
  appHashes:
  - 62df7d225d6178688f451604552fb5d79525a257ac59e281f0c02f4c96e4d343
  gitRevision: 3703a5d0543f672252f7b37e5636a9e40c3b6e5e
  verdict: nonverifiable
- date: 2024-06-04
  version: 2.1.0
  appHashes: []
  gitRevision: 49d9b9282cfd495e90fb6d839423ce6ad7b5d721
  verdict: ftbfs
twitter: SwissBitcoinPay
social:
- https://www.linkedin.com/company/swiss-bitcoin-pay
- https://www.youtube.com/@swissbitcoinpay
redirect_from: 
developerName: Swiss Bitcoin Pay
features:
- ln

---

## Update 2024-09-10:
We tested the project using our <strong>test script <a href="/testScript">(?)</a></strong>, and here is the result:
```
===== Begin Results =====
appId:          ch.swissbitcoinpay.checkout
signer:         17d9c0bf025008da16d5a146e1beaca6ddcfe3cb0cf063da23c847d3007eb621
apkVersionName: 2.1.4
apkVersionCode: 386
verdict:        
appHash:        69a7e528ca403489349a482af6a35b94646b428dd27b09336793ad5817cdd9af
commit:         6f2f989c0978da798d2c3c1b376e01ee0346a838

Diff:
Files /tmp/fromPlay_ch.swissbitcoinpay.checkout_386/AndroidManifest.xml and /tmp/fromBuild_ch.swissbitcoinpay.checkout_386/AndroidManifest.xml differ
Files /tmp/fromPlay_ch.swissbitcoinpay.checkout_386/assets/dexopt/baseline.prof and /tmp/fromBuild_ch.swissbitcoinpay.checkout_386/assets/dexopt/baseline.prof differ
Files /tmp/fromPlay_ch.swissbitcoinpay.checkout_386/assets/index.android.bundle and /tmp/fromBuild_ch.swissbitcoinpay.checkout_386/assets/index.android.bundle differ
Only in /tmp/fromPlay_ch.swissbitcoinpay.checkout_386/assets: index.android.bundle.hbc.map
Files /tmp/fromPlay_ch.swissbitcoinpay.checkout_386/assets/index.android.bundle.map and /tmp/fromBuild_ch.swissbitcoinpay.checkout_386/assets/index.android.bundle.map differ
Files /tmp/fromPlay_ch.swissbitcoinpay.checkout_386/classes.dex and /tmp/fromBuild_ch.swissbitcoinpay.checkout_386/classes.dex differ
Files /tmp/fromPlay_ch.swissbitcoinpay.checkout_386/res/drawable/notification_bg_low.xml and /tmp/fromBuild_ch.swissbitcoinpay.checkout_386/res/drawable/notification_bg_low.xml differ
Files /tmp/fromPlay_ch.swissbitcoinpay.checkout_386/res/drawable/notification_bg.xml and /tmp/fromBuild_ch.swissbitcoinpay.checkout_386/res/drawable/notification_bg.xml differ
Files /tmp/fromPlay_ch.swissbitcoinpay.checkout_386/res/drawable/notification_tile_bg.xml and /tmp/fromBuild_ch.swissbitcoinpay.checkout_386/res/drawable/notification_tile_bg.xml differ
Only in /tmp/fromBuild_ch.swissbitcoinpay.checkout_386/res/drawable-mdpi-v4: node_modules_reactnative_libraries_logbox_ui_logboximages_alerttriangle.png
Only in /tmp/fromBuild_ch.swissbitcoinpay.checkout_386/res/drawable-mdpi-v4: node_modules_reactnative_libraries_logbox_ui_logboximages_chevronleft.png
Only in /tmp/fromBuild_ch.swissbitcoinpay.checkout_386/res/drawable-mdpi-v4: node_modules_reactnative_libraries_logbox_ui_logboximages_chevronright.png
Only in /tmp/fromBuild_ch.swissbitcoinpay.checkout_386/res/drawable-mdpi-v4: node_modules_reactnative_libraries_logbox_ui_logboximages_close.png
Only in /tmp/fromBuild_ch.swissbitcoinpay.checkout_386/res/drawable-mdpi-v4: node_modules_reactnative_libraries_logbox_ui_logboximages_loader.png
Files /tmp/fromPlay_ch.swissbitcoinpay.checkout_386/res/drawable-mdpi-v4/src_assets_images_bitcointoslot.png and /tmp/fromBuild_ch.swissbitcoinpay.checkout_386/res/drawable-mdpi-v4/src_assets_images_bitcointoslot.png differ
Files /tmp/fromPlay_ch.swissbitcoinpay.checkout_386/res/drawable-mdpi-v4/src_assets_images_bitcoinwhiteborder.png and /tmp/fromBuild_ch.swissbitcoinpay.checkout_386/res/drawable-mdpi-v4/src_assets_images_bitcoinwhiteborder.png differ
Files /tmp/fromPlay_ch.swissbitcoinpay.checkout_386/res/drawable-mdpi-v4/src_assets_images_boltcardblack.png and /tmp/fromBuild_ch.swissbitcoinpay.checkout_386/res/drawable-mdpi-v4/src_assets_images_boltcardblack.png differ
Files /tmp/fromPlay_ch.swissbitcoinpay.checkout_386/res/drawable-mdpi-v4/src_assets_images_boltcard.png and /tmp/fromBuild_ch.swissbitcoinpay.checkout_386/res/drawable-mdpi-v4/src_assets_images_boltcard.png differ
Files /tmp/fromPlay_ch.swissbitcoinpay.checkout_386/res/drawable-mdpi-v4/src_assets_images_logosquarerounded.png and /tmp/fromBuild_ch.swissbitcoinpay.checkout_386/res/drawable-mdpi-v4/src_assets_images_logosquarerounded.png differ
Files /tmp/fromPlay_ch.swissbitcoinpay.checkout_386/res/mipmap-hdpi-v4/ic_launcher_adaptive_fore.png and /tmp/fromBuild_ch.swissbitcoinpay.checkout_386/res/mipmap-hdpi-v4/ic_launcher_adaptive_fore.png differ
Files /tmp/fromPlay_ch.swissbitcoinpay.checkout_386/resources.arsc and /tmp/fromBuild_ch.swissbitcoinpay.checkout_386/resources.arsc differ
Only in /tmp/fromPlay_ch.swissbitcoinpay.checkout_386: stamp-cert-sha256

Revision, tag (and its signature):

===== End Results =====
```

The app built successfully although many of the differences consisted of timestamps and file permission codes.

The amount of diffs indicate that the app is **nonverifiable.** 

We raised an issue regarding this in SwissBitcoinPay's [issue tracker.](https://github.com/SwissBitcoinPay/app/issues/107)




