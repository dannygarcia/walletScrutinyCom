---
wsId: 
title: Bitcoin Keeper
altTitle: 
authors: 
users: 1000
appId: io.hexawallet.bitcoinkeeper
appCountry: 
released: 2022-12-12
updated: 2025-01-12
version: 1.3.3
stars: 4.8
ratings: 
reviews: 3
size: 
website: https://www.bitcoinkeeper.app/
repository: 
issue: 
icon: io.hexawallet.bitcoinkeeper.png
bugbounty: 
meta: ok
verdict: wip
appHashes: 
date: 2023-06-03
signer: 
reviewArchive: 
twitter: 
social: 
redirect_from: 
developerName: BitHyve UK Ltd.
features: 

---

