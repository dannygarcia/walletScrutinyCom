---
wsId: quantfury
title: 'Quantfury: Your Global Broker'
altTitle: 
authors:
- danny
appId: com.quantfury
appCountry: br
idd: 1445564443
released: 2018-12-15
updated: 2025-01-23
version: 1.84.0
stars: 3.2
reviews: 45
size: '72471552'
website: https://quantfury.com/
repository: 
issue: 
icon: com.quantfury.jpg
bugbounty: 
meta: ok
verdict: custodial
appHashes: 
date: 2024-10-07
signer: 
reviewArchive: 
twitter: quantfury
social: 
features: 
developerName: Quantfury Ltd

---

{% include copyFromAndroid.html %}
