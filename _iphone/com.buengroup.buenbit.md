---
wsId: buenbit
title: 'Buenbit: Invierte en el futuro'
altTitle: 
authors:
- danny
appId: com.buengroup.buenbit
appCountry: co
idd: '1552402029'
released: 2021-11-23
updated: 2025-01-24
version: 3.26.0
stars: 4.9
reviews: 14
size: '113757184'
website: https://www.buenbit.com/
repository: 
issue: 
icon: com.buengroup.buenbit.jpg
bugbounty: 
meta: ok
verdict: custodial
appHashes: 
date: 2023-12-14
signer: 
reviewArchive: 
twitter: 
social: 
features: 
developerName: Buenbit

---

{% include copyFromAndroid.html %}