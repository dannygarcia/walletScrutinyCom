---
wsId: CoinbaseWallet
title: 'Coinbase Wallet: NFTs & Crypto'
altTitle: 
authors:
- leo
appId: org.toshi.distribution
appCountry: 
idd: 1278383455
released: 2017-09-27
updated: 2025-02-03
version: '29.28'
stars: 4.6
reviews: 156111
size: '201398272'
website: https://www.coinbase.com/wallet
repository: 
issue: 
icon: org.toshi.distribution.jpg
bugbounty: 
meta: ok
verdict: nosource
appHashes: 
date: 2021-01-04
signer: 
reviewArchive: 
twitter: CoinbaseWallet
social: 
features: 
developerName: Coinbase Wallet

---

This is the iPhone version of the Android
{% include walletLink.html wallet='android/org.toshi' %}.

Just like the Android version, this wallet is **not verifiable**.
