---
wsId: biconomy
title: Biconomy
altTitle: 
authors:
- danny
appId: com.biconomy.Innovation
appCountry: us
idd: '1486151349'
released: 2019-11-06
updated: 2025-01-21
version: 2.2.19
stars: 4.3
reviews: 97
size: '90651648'
website: http://www.biconomy.com/
repository: 
issue: 
icon: com.biconomy.Innovation.jpg
bugbounty: 
meta: ok
verdict: custodial
appHashes: 
date: 2023-03-29
signer: 
reviewArchive: 
twitter: BiconomyCom
social:
- https://www.facebook.com/BiconomyCom
- https://www.linkedin.com/company/biconomycom/
- https://biconomycom.medium.com/
- https://www.youtube.com/c/BiconomyGlobal
features: 
developerName: Biconomy Ltd.

---

{% include copyFromAndroid.html %}
