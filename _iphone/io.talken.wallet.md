---
wsId: Talken
title: Talken Wallet
altTitle: 
authors:
- kiwilamb
appId: io.talken.wallet
appCountry: 
idd: 1459475831
released: 2019-09-25
updated: 2025-01-31
version: 2.00.01
stars: 5
reviews: 6
size: '101249024'
website: https://talken.io/
repository: 
issue: 
icon: io.talken.wallet.jpg
bugbounty: 
meta: ok
verdict: nobtc
appHashes: 
date: 2021-06-04
signer: 
reviewArchive: 
twitter: Talken_
social: 
features: 
developerName: Colligence Inc.

---

{% include copyFromAndroid.html %}

