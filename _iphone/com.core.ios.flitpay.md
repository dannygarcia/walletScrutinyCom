---
wsId: flitpay
title: 'Flitpay: Crypto Exchange'
altTitle: 
authors:
- danny
appId: com.core.ios.flitpay
appCountry: in
idd: 1571975471
released: 2021-08-27
updated: 2024-08-23
version: 1.0.22
stars: 4
reviews: 80
size: '34112512'
website: https://www.flitpay.com
repository: 
issue: 
icon: com.core.ios.flitpay.jpg
bugbounty: 
meta: ok
verdict: custodial
appHashes: 
date: 2024-10-07
signer: 
reviewArchive: 
twitter: flitpayofficial
social:
- https://www.facebook.com/flitpay
features: 
developerName: FLITPAY PVT LTD

---

{% include copyFromAndroid.html %}
