---
wsId: trastra
title: 'TRASTRA: Use USDT with Card'
altTitle: 
authors:
- danny
appId: com.trastra.mobile
appCountry: de
idd: 1446427008
released: 2019-01-20
updated: 2025-01-22
version: 5.0.8
stars: 5
reviews: 1
size: '61000704'
website: https://mobile-app.trastra.com/
repository: 
issue: 
icon: com.trastra.mobile.jpg
bugbounty: 
meta: ok
verdict: custodial
appHashes: 
date: 2024-10-07
signer: 
reviewArchive: 
twitter: Trastra_ltd
social:
- https://www.linkedin.com/company/trastra
- https://www.facebook.com/trastra.ltd
features: 
developerName: Trastra Limited

---

**Update 2022-02-26**: This app is not available anymore.

{% include copyFromAndroid.html %}
