---
wsId: fountainPodcasts
title: 'Fountain: Podcasts & Music'
altTitle: 
authors:
- danny
appId: fm.fountain.apps
appCountry: ph
idd: '1576394424'
released: 2021-07-28
updated: 2025-01-30
version: 1.1.15
stars: 3
reviews: 2
size: '121307136'
website: https://www.fountain.fm
repository: 
issue: 
icon: fm.fountain.apps.jpg
bugbounty: 
meta: ok
verdict: custodial
appHashes: 
date: 2023-07-24
signer: 
reviewArchive: 
twitter: fountain_app
social: 
features: 
developerName: Fountain Labs Ltd.

---

{% include copyFromAndroid.html %}