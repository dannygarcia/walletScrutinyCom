---
wsId: whitebit
title: WhiteBIT – buy & sell bitcoin
altTitle: 
authors:
- danny
appId: com.whitebit.whitebitapp
appCountry: ua
idd: 1463405025
released: 2019-05-21
updated: 2025-01-30
version: 3.37.2
stars: 4.7
reviews: 2217
size: '102487040'
website: https://whitebit.com
repository: 
issue: 
icon: com.whitebit.whitebitapp.jpg
bugbounty: 
meta: ok
verdict: custodial
appHashes: 
date: 2021-09-10
signer: 
reviewArchive: 
twitter: whitebit
social:
- https://www.linkedin.com/company/whitebit-cryptocurrency-exchange
- https://www.facebook.com/whitebit
- https://www.reddit.com/r/WhiteBitExchange
features: 
developerName: UAB Clear White Technologies

---

{% include copyFromAndroid.html %}