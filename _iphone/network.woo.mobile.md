---
wsId: wooXTrading
title: 'WOO X: Trade Bitcoin & ETH'
altTitle: 
authors:
- danny
appId: network.woo.mobile
appCountry: ph
idd: '1576648404'
released: 2021-09-17
updated: 2025-01-24
version: 3.47.2
stars: 5
reviews: 4
size: '129571840'
website: https://woox.io/
repository: 
issue: 
icon: network.woo.mobile.jpg
bugbounty: 
meta: ok
verdict: custodial
appHashes: 
date: 2023-04-15
signer: 
reviewArchive: 
twitter: 
social: 
features: 
developerName: Wootech Limited

---

{% include copyFromAndroid.html %}
