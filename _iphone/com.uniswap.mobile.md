---
wsId: uniswap
title: 'Uniswap: Crypto & NFT Wallet'
altTitle: 
authors:
- danny
appId: com.uniswap.mobile
appCountry: us
idd: '6443944476'
released: 2023-04-12
updated: 2025-01-30
version: 1.44.1
stars: 4.8
reviews: 14594
size: '73895936'
website: https://wallet.uniswap.org/
repository: 
issue: 
icon: com.uniswap.mobile.jpg
bugbounty: 
meta: ok
verdict: nobtc
appHashes: 
date: 2024-09-05
signer: 
reviewArchive: 
twitter: uniswap
social:
- https://discord.com/invite/FCfyBSbCU5
features: 
developerName: Uniswap Labs

---

{% include copyFromAndroid.html %}
