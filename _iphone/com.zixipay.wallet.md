---
wsId: ZixiPay
title: 'ZixiPay: Tether Wallet'
altTitle: 
authors:
- danny
appId: com.zixipay.wallet
appCountry: us
idd: 1492139262
released: 2019-12-22
updated: 2024-12-07
version: '1.89'
stars: 4.1
reviews: 17
size: '66570240'
website: https://zixipay.com/
repository: 
issue: 
icon: com.zixipay.wallet.jpg
bugbounty: 
meta: ok
verdict: custodial
appHashes: 
date: 2021-11-17
signer: 
reviewArchive: 
twitter: zixipay
social:
- https://www.facebook.com/ZixiPay
features: 
developerName: ZixiPay LLC

---

{% include copyFromAndroid.html %}
