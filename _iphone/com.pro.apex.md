---
wsId: apexProTrade
title: 'ApeX Protocol: Trade Crypto'
altTitle: 
authors:
- danny
appId: com.pro.apex
appCountry: us
idd: '1645456064'
released: 2022-09-27
updated: 2025-01-27
version: 3.6.1
stars: 4
reviews: 59
size: '102010880'
website: 
repository: 
issue: 
icon: com.pro.apex.jpg
bugbounty: 
meta: ok
verdict: nowallet
appHashes: 
date: 2023-07-02
signer: 
reviewArchive: 
twitter: OfficialApeXdex
social:
- https://apex.exchange
- https://apexdex.medium.com
- https://discord.com/invite/366Puqavwx
- https://t.me/ApeXdex
features: 
developerName: APEX DAO LLC

---

{% include copyFromAndroid.html %}
