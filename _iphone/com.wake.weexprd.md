---
wsId: weexTrade
title: 'WEEX: Trade Bitcoin & Futures'
altTitle: 
authors:
- danny
appId: com.wake.weexprd
appCountry: us
idd: '1609350789'
released: 2022-03-04
updated: 2025-01-24
version: 4.3.3
stars: 4.6
reviews: 469
size: '107852800'
website: https://www.weex.com/en
repository: 
issue: 
icon: com.wake.weexprd.jpg
bugbounty: 
meta: ok
verdict: custodial
appHashes: 
date: 2023-08-17
signer: 
reviewArchive: 
twitter: WEEX_Official
social:
- https://www.facebook.com/WEEXGlobal
- https://t.me/Weex_Global
- https://www.linkedin.com/company/weex-global
features: 
developerName: Wake Co., Ltd.

---

{% include copyFromAndroid.html %}