---
wsId: bitpie
title: Bitpie-Universal Crypto Wallet
altTitle: 
authors:
- leo
appId: com.bitpie.wallet
appCountry: 
idd: 1481314229
released: 2019-10-01
updated: 2024-12-30
version: 5.0.182
stars: 3.4
reviews: 277
size: '181159936'
website: https://bitpie.com
repository: 
issue: 
icon: com.bitpie.wallet.jpg
bugbounty: 
meta: ok
verdict: nosource
appHashes: 
date: 2021-10-01
signer: 
reviewArchive: 
twitter: BitpieWallet
social:
- https://www.facebook.com/BitpieOfficial
- https://www.reddit.com/r/BitpieWallet
features:
- ln
developerName: BITPIE HK LIMITED

---

 {% include copyFromAndroid.html %}
