---
wsId: jgCapital
title: J G Capital
altTitle: 
authors:
- danny
appId: com.suffescom.jeangilles
appCountry: us
idd: '1642934824'
released: 2022-09-16
updated: 2024-11-26
version: 1.6.0
stars: 3.7
reviews: 15
size: '16731136'
website: 
repository: 
issue: 
icon: com.suffescom.jeangilles.jpg
bugbounty: 
meta: ok
verdict: nowallet
appHashes: 
date: 2023-07-18
signer: 
reviewArchive: 
twitter: 
social:
- https://jgcapitalbitcoin.com
- https://www.facebook.com/jeangillescapital
features: 
developerName: Jean Gilles Capital LLC

---

{% include copyFromAndroid.html %}