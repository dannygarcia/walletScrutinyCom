---
wsId: krakenSuperWallet
title: 'Kraken Wallet: Crypto & DeFi'
altTitle: 
authors:
- danny
appId: com.kraken.superwallet.app
appCountry: us
idd: '1626327149'
released: '2024-04-17'
updated: 2025-01-28
version: 1.16.1
stars: 4.7
reviews: 299
size: '99396608'
website: https://www.kraken.com/wallet
repository: https://github.com/krakenfx/wallet
issue: https://github.com/krakenfx/wallet/issues/53
icon: com.kraken.superwallet.app.jpg
bugbounty: 
meta: ok
verdict: nonverifiable
appHashes: 
date: 2025-02-04
signer: 
reviewArchive: 
twitter: krakenfx
social:
- https://www.linkedin.com/company/krakenfx
- https://www.facebook.com/KrakenFX
- https://www.reddit.com/r/Kraken
features: 
developerName: Kraken

---

{% include copyFromAndroid.html %}