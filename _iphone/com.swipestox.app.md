---
wsId: swipestoxNaga
title: 'NAGA: Invest in Forex & Stocks'
altTitle: 
authors:
- danny
appId: com.swipestox.app
appCountry: in
idd: 1182702365
released: 2017-01-15
updated: 2025-02-04
version: 9.1.15
stars: 4.7
reviews: 49
size: '170739712'
website: https://www.naga.com
repository: 
issue: 
icon: com.swipestox.app.jpg
bugbounty: 
meta: ok
verdict: nosendreceive
appHashes: 
date: 2024-10-07
signer: 
reviewArchive: 
twitter: nagainvesting
social:
- https://www.linkedin.com/company/nagainvesting
- https://www.facebook.com/nagainvesting
features: 
developerName: NAGA Markets Ltd.

---

{% include copyFromAndroid.html %}
