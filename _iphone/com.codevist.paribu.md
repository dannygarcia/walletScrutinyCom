---
wsId: paribu
title: Paribu | Bitcoin Alım Satım
altTitle: 
authors:
- danny
appId: com.codevist.paribu
appCountry: 
idd: 1448200352
released: 2019-05-29
updated: 2025-01-28
version: 5.0.15
stars: 4.3
reviews: 192
size: '50715648'
website: https://www.paribu.com
repository: 
issue: 
icon: com.codevist.paribu.jpg
bugbounty: 
meta: ok
verdict: custodial
appHashes: 
date: 2023-12-16
signer: 
reviewArchive: 
twitter: paribucom
social:
- https://www.youtube.com/c/Paribu
- https://www.linkedin.com/company/paribucom/
- https://www.instagram.com/paribucom/
- https://www.facebook.com/paribucom
features: 
developerName: Paribu Teknoloji A.Ş.

---

{% include copyFromAndroid.html %}