---
wsId: CoinJar
title: 'CoinJar: Buy Bitcoin Instantly'
altTitle: 
authors:
- danny
appId: com.coinjar.mobius
appCountry: au
idd: 958797429
released: 2015-02-04
updated: 2025-01-30
version: 3.11.0
stars: 4.7
reviews: 14511
size: '68654080'
website: https://www.coinjar.com
repository: 
issue: 
icon: com.coinjar.mobius.jpg
bugbounty: 
meta: ok
verdict: custodial
appHashes: 
date: 2021-09-15
signer: 
reviewArchive: 
twitter: getcoinjar
social:
- https://www.linkedin.com/company/coinjar
- https://www.facebook.com/CoinJar
features: 
developerName: CoinJar Pty Ltd

---

{% include copyFromAndroid.html %}