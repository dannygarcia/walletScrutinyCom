---
wsId: ajaibkripto
title: Ajaib Alpha (Ajaib Kripto)
altTitle: 
authors:
- danny
appId: id.ajaib-coin.co
appCountry: id
idd: '1634168301'
released: 2022-08-16
updated: 2025-01-21
version: 2.65.0
stars: 4.3
reviews: 2273
size: '157526016'
website: https://alpha.ajaib.co.id/
repository: 
issue: 
icon: id.ajaib-coin.co.jpg
bugbounty: 
meta: ok
verdict: custodial
appHashes: 
date: 2024-09-06
signer: 
reviewArchive: 
twitter: 
social: 
features: 
developerName: Ajaib Technologies Corporation

---

{% include copyFromAndroid.html %}