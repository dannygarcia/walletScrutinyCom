---
wsId: globianceApp
title: Globiance
altTitle: 
authors:
- danny
appId: com.globiance.iosapp
appCountry: us
idd: '1584923932'
released: 2021-10-05
updated: 2024-07-05
version: '2.07'
stars: 4.2
reviews: 86
size: '59216896'
website: https://globiance.com
repository: 
issue: 
icon: com.globiance.iosapp.jpg
bugbounty: 
meta: ok
verdict: custodial
appHashes: 
date: 2023-07-20
signer: 
reviewArchive: 
twitter: globiance
social:
- https://www.facebook.com/Globiance
- https://www.youtube.com/c/GLOBIANCE
- https://www.instagram.com/globiance
- https://t.me/globiancegroup
- https://www.linkedin.com/company/globiance
features: 
developerName: Globiance Holdings Limited

---

{% include copyFromAndroid.html %}