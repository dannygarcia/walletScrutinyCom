---
wsId: goabra
title: 'Abra: Buy & Trade BTC & Crypto'
altTitle: 
authors:
- leo
appId: com.goabra.abra
appCountry: jp
idd: 966301394
released: 2015-03-12
updated: 2024-02-16
version: 145.0.0
stars: 4.7
reviews: 161
size: '221251584'
website: 
repository: 
issue: 
icon: com.goabra.abra.jpg
bugbounty: 
meta: ok
verdict: custodial
appHashes: 
date: 2024-10-07
signer: 
reviewArchive: 
twitter: AbraGlobal
social:
- https://www.linkedin.com/company/abra
- https://www.facebook.com/GoAbraGlobal
features: 
developerName: Plutus Financial

---

This is the iPhone version of the Android
{% include walletLink.html wallet='android/com.plutus.wallet' %}.

Just like the Android version, this wallet is **not verifiable**.
