---
wsId: blockchainWallet
title: 'Blockchain.com: Crypto Wallet'
altTitle: 
authors:
- leo
appId: com.rainydayapps.Blockchain
appCountry: 
idd: 493253309
released: 2012-04-13
updated: 2025-01-31
version: 202501.2.2
stars: 4.7
reviews: 174375
size: '265801728'
website: https://www.blockchain.com/wallet
repository: https://github.com/blockchain/My-Wallet-V3-iOS
issue: 
icon: com.rainydayapps.Blockchain.jpg
bugbounty: 
meta: ok
verdict: nosource
appHashes: 
date: 2021-07-15
signer: 
reviewArchive: 
twitter: Blockchain
social:
- https://www.linkedin.com/company/blockchain
- https://www.facebook.com/Blockchain
features: 
developerName: Blockchain

---

{% include copyFromAndroid.html %}