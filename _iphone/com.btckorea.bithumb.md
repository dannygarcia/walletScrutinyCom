---
wsId: bithumbko
title: Bithumb
altTitle: 
authors:
- leo
appId: com.btckorea.bithumb
appCountry: kr
idd: 1299421592
released: 2017-12-05
updated: 2025-01-23
version: 2.6.5
stars: 2
reviews: 3442
size: '307236864'
website: https://en.bithumb.com
repository: 
issue: 
icon: com.btckorea.bithumb.jpg
bugbounty: 
meta: ok
verdict: custodial
appHashes: 
date: 2024-10-07
signer: 
reviewArchive: 
twitter: BithumbOfficial
social:
- https://www.facebook.com/bithumb
features: 
developerName: Bithumb Co., Ltd.

---

This app is an interface to an exchange and to our knowledge only features
custodial accounts and therefore is **not verifiable**.
